﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Net;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Win32;
using HttpVideoStreamer.Http;

namespace HttpVideoStreamer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        HttpReceiver _receiver;
        HttpSender _sender;
        VideoStreamController _videoController;

        // The uri pointing to the receiver 
        string _fileName;
        int _receiverPort;


        #region Constructors
        public MainWindow()
        {
            InitializeComponent();

            // Event binding.
            mdElem_videoBox.MediaFailed += (s, e) => Utils.Log(mdElem_videoBox, "VideoBox: media failed.", e.ErrorException.ToString());
            Utils.OnLogging += (s, e) => Log("Receiver: " + e);

            // Ports used by each host
            _receiverPort = 8081;
            int senderPort = 8080;

            // Initializing prefixes. Each object will listen for connections on the given prefix
            _receiver = new HttpReceiver(new string[] { "http://+:" + _receiverPort + "/" });
            _receiver.OnHttpPost += HttpPostHandler;

            _sender = new HttpSender(new string[] { "http://+:" + senderPort + "/" }, senderPort);

            lbl_senderPrefix.Content = "Sender prefix:\thttp://" + Utils.GetLocalAddress() + ":8080/";

            this.Closed += (s, e) => Dispose();
        }

        ~MainWindow()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_receiver != null)
                _receiver.Dispose();

            if (_sender != null)
                _sender.Dispose();

            if (_videoController != null)
                _videoController.Dispose();
        }
        #endregion


        #region Event handlers
        /// <summary>
        /// Handler for the video command received.
        /// </summary>
        /// <param name="context">The context of the http request.</param>
        /// <param name="uri">Video source.</param>
        private void HttpPostHandler(object context, Uri uri)
        {
            this.Dispatcher.InvokeAsync(delegate ()
            {
                try
                {
                    mdElem_videoBox.Source = uri;
                    mdElem_videoBox.Play();
                }
                catch(Exception e)
                {
                    Utils.Log(context, e.ToString(), "\n\nUri: ", uri.ToString());
                }
            });
        }
        #endregion


        #region Utils methods
        private void InterfaceLog(params object[] objects)
        {
            Log("Interface: ", string.Concat(objects));
        }

        private void Log(params object[] objects)
        {
            string str = string.Empty;

            foreach (object o in objects)
            {
                str += o.ToString();
            }

            this.Dispatcher.InvokeAsync(delegate ()
            {
                lstBox_logger.Items.Add(str);
            });
        }
        #endregion


        #region Private methods
        private void LoadFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if ((bool)dialog.ShowDialog())
            {
                _fileName = dialog.FileName;
                InterfaceLog("File selected.");
            }
            else
                InterfaceLog("Opening aborted.");
        }


        private void ChangeListenStatus()
        {
            if(_receiver.IsListening)
            {
                InterfaceLog("Stopping receiver...");
                _receiver.Stop();
            }
            else
            {
                InterfaceLog("Starting receiver...");
                _receiver.Start();
            }
        }


        private void Send()
        {
            if (_fileName == string.Empty)
                InterfaceLog("No file selected.");
            else
            {
                string ip = string.Empty;

                this.Dispatcher.Invoke(delegate ()
                {
                    // Disables the button, to prevent interrupting the stream
                    btn_send.IsEnabled = false;
                    ip = txtBox_receiverIP.Text;
                });

                InterfaceLog("Uploading file...");
                try
                {
                    IVideoStreamController videoHandler = _sender.SendVideo(new Uri(Uri.UriSchemeHttp + "://" + ip + ":" + _receiverPort.ToString() + "/", UriKind.Absolute), _fileName.Remove(0, 2));
                    
                    // Waits the task to finish...
                    InterfaceLog("File uploading result: \"" + videoHandler.Status.ToString() + "\"");

                    if (videoHandler.Status != TaskStatus.Faulted)
                    {
                        VideoStreamController videoController = (VideoStreamController)videoHandler;
                        videoController.Play();
                        _videoController = videoController;
                    }

                }
                catch (Exception e)
                {
                    InterfaceLog("ERROR: ", e.ToString());
                }
                finally
                {
                    this.Dispatcher.Invoke(delegate ()
                    {
                        // Ensures to re-enable the button
                        btn_send.IsEnabled = true;
                    });
                }
            }
        }
        #endregion


        #region Buttons
        private void Btn_listen_Click(object sender, RoutedEventArgs e)
        {
            Action a = ChangeListenStatus;
            a.BeginInvoke((ar) => a.EndInvoke(ar), null);
        }

        private void Btn_load_Click(object sender, RoutedEventArgs e)
        {
            Action a = LoadFile;
            a.BeginInvoke((ar) => a.EndInvoke(ar), null);
        }

        private void Btn_send_Click(object sender, RoutedEventArgs e)
        {
            Action a = Send;
            a.BeginInvoke((ar) => a.EndInvoke(ar), null);
        }


        private void Btn_play_Click(object sender, RoutedEventArgs e)
        {
            if (_videoController != null && !_videoController.IsPlaying)
                _videoController.Play();
        }

        private void Btn_pause_Click(object sender, RoutedEventArgs e)
        {
            if (_videoController != null && _videoController.IsPlaying)
                _videoController.Stop();
        }

        private void Btn_stop_Click(object sender, RoutedEventArgs e)
        {
            if (_videoController != null)
                _videoController.Stop();
        }
        #endregion
    }
}
