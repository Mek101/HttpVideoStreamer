﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

namespace HttpVideoStreamer.Http
{
    class HttpSender : IDisposable
    {
        HttpClient _client;
        HttpListener _listener;

        public int LocalPort { get; private set; }
        public IPAddress LocalAddress { get; private set; }


        #region Constructor
        public HttpSender(IEnumerable<Uri> prefixes, int localPort) : this(prefixes.Select<Uri, string>((u) => u.AbsoluteUri), localPort) { }

        public HttpSender(IEnumerable<string> prefixes, int localPort)
        {
            if (prefixes.Count() < 1)
                throw new ArgumentException("The sender requires a prefix to listen to.");

            LocalPort = localPort;
            LocalAddress = Utils.GetLocalAddress();

            _client = new HttpClient();
            _client.BaseAddress = new Uri("http://" + LocalAddress.ToString() + ":" + localPort.ToString() + "/");

            _listener = new HttpListener();
            foreach (string prefix in prefixes)
                _listener.Prefixes.Add(prefix.ToString());
        }

        ~HttpSender()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_listener != null)
                _listener.Abort();

            if (_client != null)
                _client.Dispose();

            _listener = null;
            _client = null;
            LocalAddress = null;
        }
        #endregion


        #region Public send methods
        /// <summary>
        /// Sends to the given destination the command to request the given video.
        /// </summary>
        /// <param name="destination">Destination Uri.</param>
        /// <param name="videoSource">Path to the local video.</param>
        /// <param name="videoPort">Port through the receiver should request the video.</param>
        /// <returns>The receiver status.</returns>
        public IVideoStreamController SendVideo(Uri destination, string videoSource)
        {
            return SendVideoAsync(destination, videoSource).Result;
        }


        /// <summary>
        /// Sends to the given destination the command to request the given video asyncronously.
        /// </summary>
        /// <param name="destination">Destination Uri.</param>
        /// <param name="videoSource">Path to the local video.</param>
        /// <param name="videoPort">Port through the receiver should request the video.</param>
        /// <returns>The receiver status.</returns>
        public async Task<IVideoStreamController> SendVideoAsync(Uri destination, string videoPath)
        {
            if (videoPath.Contains(' '))
                throw new ArgumentException("Unsupported file name or path, contains ' '.");

            // Starts listening now to prevent missing requests
            _listener.Start();
            Utils.Log(this, "sending command.");

            Uri videoUrl = GetVideoUri(videoPath);

            HttpResponseMessage response = await _client.PostAsync(destination, new StringContent(videoUrl.ToString()));

            return HandleVideoRequest(response, videoPath);
        }
        #endregion


        private IVideoStreamController HandleVideoRequest(HttpResponseMessage response, string videoSource)
        {
            IVideoStreamController controller = null;

            // If the receiver response was positive
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                // Listens for the receiver video request
                HttpListenerContext context = _listener.GetContext(); // Waits...

                VideoStreamController temp = new VideoStreamController(context, videoSource);
                temp.OnStreamTerminated += (s, e) => _listener.Stop();
                controller = temp;
            }
            else
            {
                Utils.Log(this, "Unattended response: ", response.StatusCode.ToString());

                controller = new VideoStreamFailed((int)response.StatusCode);
            }

            return controller;
        }


        /// <summary>
        /// Builds the uri pointing to the video.
        /// </summary>
        /// <param name="videoPath">Path to the video file.</param>
        /// <returns>Network uri pointing to the video.</returns>
        private Uri GetVideoUri(string videoPath)
        {
            UriBuilder builder = new UriBuilder(Uri.UriSchemeHttp, LocalAddress.ToString(), LocalPort, videoPath);
            return new Uri(Uri.EscapeUriString(builder.Uri.ToString()), UriKind.Absolute);
        }
    }
}
