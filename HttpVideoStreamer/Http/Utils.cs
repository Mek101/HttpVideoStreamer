﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace HttpVideoStreamer.Http
{
    public static class Utils
    {
        // Cache
        private static IPAddress _lastAddress;

        public static IPAddress GetLocalAddress() => GetLocalAddress(false);

        public static IPAddress GetLocalAddress(bool force)
        {
            if (_lastAddress == null || force)
            {
                using (Socket sck = new Socket(SocketType.Dgram, ProtocolType.Udp))
                {
                    sck.Connect("1.1.1.1", 65530);
                    IPEndPoint endPoint = sck.LocalEndPoint as IPEndPoint;

                    if (endPoint == null)
                        throw new Exception("Unable to get local address.");
                    else
                    {
                        _lastAddress = endPoint.Address;
                        if (_lastAddress.IsIPv4MappedToIPv6)
                            _lastAddress = _lastAddress.MapToIPv4();
                    }
                }
            }

            return _lastAddress;
        }


        /// <summary>
        /// Extracts the contents from a stream to string with the given encoding.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string GetContent(Stream stream, Encoding encoding)
        {
            List<byte> buffer = new List<byte>(48);
            byte[] tempBuffer = new byte[48];
            int read = 0;

            while (true)
            {
                read = stream.Read(tempBuffer, 0, tempBuffer.Length);

                if (read == tempBuffer.Length)
                    buffer.AddRange(tempBuffer);
                else
                {
                    for (int i = 0; i < read; i++)
                        buffer.Add(tempBuffer[i]);
                    break;
                }
            }

            return encoding.GetString(buffer.ToArray());
        }



        public static event EventHandler<string> OnLogging;

        public static void Log(object sender, params string[] text)
        {
            if (OnLogging != null && text != null && text.Length > 0)
                OnLogging(sender, string.Concat(text));
        }
    }
}
