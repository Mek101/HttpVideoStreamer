﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

namespace HttpVideoStreamer.Http
{
    class VideoStreamController : IVideoStreamController, IDisposable
    {
        HttpListenerContext _httpContext;
        string _videoPath;
        const int BUFFER_SIZE = 4000;

        // Task stuff
        Task<int> _operation;
        CancellationTokenSource _tokenSource;
        CancellationToken _token;

        // Public proprieties
        public bool IsPlaying { get { return _operation.Status == TaskStatus.Running; /*_syncLock.WaitOne(0, true);*/ } }
        public TaskStatus Status => _operation.Status;

        public event EventHandler OnStreamTerminated;


        #region Construct
        public VideoStreamController(HttpListenerContext httpContext, string videoPath)
        {
            _httpContext = httpContext;
            _videoPath = videoPath;

            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;
            _operation = Task.Factory.StartNew<int>(Send, TaskCreationOptions.LongRunning);
        }

        ~VideoStreamController()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_tokenSource != null)
            {
                _tokenSource.Cancel();
                Play();

                _tokenSource.Dispose();
                _tokenSource = null;
            }

            if (_operation != null)
            {
                _operation.Dispose();
                _operation = null;
            }

            if (_httpContext != null)
            {
                if (_httpContext.Response != null)                
                    _httpContext.Response.Abort();

                _httpContext = null;
            }

            OnStreamTerminated = null;
        }
        #endregion


        #region Stream control
        /// <summary>
        /// Starts the streaming.
        /// </summary>
        public void Play()
        {
            Utils.Log(this, "Starting video stream.");

        }


        /// <summary>
        /// Blocks the streaming.
        /// </summary>
        public void Pause()
        {
            Utils.Log(this, "Pausing video stream.");

        }


        /// <summary>
        /// Aborts the streaming.
        /// </summary>
        public void Stop()
        {
            if (!_token.IsCancellationRequested)
            {
                Utils.Log(this, "Stopping video stream.");
                _tokenSource.Cancel();
                Play();
            }
        }
        #endregion


        /// <summary>
        /// Blocks the calling thread until the operations finishes, then returns the result.
        /// </summary>
        /// <returns></returns>
        public int GetResult()
        {
            return _operation.Result;
        }


        private int Send()
        {
            // Code to return
            int statusCode = (int)HttpStatusCode.InternalServerError;
           
            // Checks the request method
            if (_httpContext.Request.HttpMethod == HttpMethod.Get.Method)
            {
                int outputByteCount = 0;
                Utils.Log(this, "sending video...");

                // Loads file to the buffer response.
                using (FileStream fileStream = new FileStream(_videoPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    byte[] buffer = new byte[BUFFER_SIZE];
                    _httpContext.Response.ContentLength64 = fileStream.Length;

                    // Breaks the cycle if the operation is canceled or the data lentgh of the streams is still different
                    while (!_token.IsCancellationRequested || outputByteCount >= fileStream.Length)
                    {
                        // Copies 4KB of data to the buffer
                        int read = fileStream.Read(buffer, 0, BUFFER_SIZE);
                        _httpContext.Response.OutputStream.Write(buffer, 0, read);

                        outputByteCount += read;
                    }
                }

                statusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                Utils.Log(this, "Unattended method: ", _httpContext.Request.HttpMethod);
                // Returns an error, wrong method.
                statusCode = (int)HttpStatusCode.MethodNotAllowed;
                _httpContext.Response.StatusDescription = "Unattended method.";
            }

            // Success, closes the context, sending the response
            _httpContext.Response.StatusCode = statusCode;
            _httpContext.Response.Close();

            if (OnStreamTerminated != null)
                OnStreamTerminated(this, new EventArgs());
            
            return statusCode;
        }
    }


    class VideoStreamFailed : IVideoStreamController
    {
        int _code;

        public VideoStreamFailed(int code)
        {
            _code = code;
        }

        public TaskStatus Status { get { return TaskStatus.Faulted; } }

        public int GetResult() { return _code; }
    }
}
