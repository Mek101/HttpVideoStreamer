﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;

namespace HttpVideoStreamer.Http
{
    class HttpReceiver : IDisposable
    {
        Thread _worker;        
        HttpListener _httpListener;
        bool _keepListening;

        public bool IsListening => _httpListener.IsListening;
        public event EventHandler<Uri> OnHttpPost;

        #region Constructor
        public HttpReceiver(IEnumerable<Uri> prefixes) : this(prefixes.Select<Uri, string>((u) => u.AbsoluteUri)) { }

        public HttpReceiver(IEnumerable<string> prefixes)
        {
            _httpListener = new HttpListener();
            foreach (string prefix in prefixes)
                _httpListener.Prefixes.Add(prefix);

            _keepListening = false;
        }


        ~HttpReceiver()
        {
            Dispose();
        }


        public void Dispose()
        {
            _keepListening = false;
            if (_httpListener != null)
                _httpListener.Abort();

            if (_worker != null)
                _worker.Abort();

            _worker = null;
            _httpListener = null;
            OnHttpPost = null;
        }
        #endregion


        #region Control
        public void Start()
        {
            _keepListening = true;
            _worker = new Thread(ThreadMain);
            _worker.Start();
        }


        public void Stop()
        {
            _keepListening = false;
            _httpListener.Stop();

            if (_worker != null)
                _worker.Join();

            _worker = null;
        }


        public void Abort()
        {
            _keepListening = false;
            _worker.Abort();
            _worker = null;
        }


        public async void StopAsync()
        {
            await Task.Factory.StartNew(Stop);
        }
        #endregion


        private void ThreadMain()
        {
            Utils.Log(this, "Starting server.");

            _httpListener.Start();

            while (_keepListening)
            {
                try
                {
                    HttpListenerContext context = _httpListener.GetContext();
                    Task.Factory.StartNew(HttpPostContextHandler, context);
                }
                // Stopping the operation while en-course causes and exception
                catch(HttpListenerException e)
                {
                    // If the error has another origin
                    if (_keepListening)
                        throw e;
                }                                
            }

            Utils.Log(this, "Stopping server.");

            if(_httpListener.IsListening)
                _httpListener.Stop();
        }


        private void HttpPostContextHandler(object container)
        {
            // Extracts
            HttpListenerContext context = (HttpListenerContext)container;
            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;

            Utils.Log(this, "Request method: ", HttpMethod.Post.Method);

            // Checks the request method
            if (request.HttpMethod == HttpMethod.Post.Method)
            {
                string content = Utils.GetContent(request.InputStream, request.ContentEncoding);

                Utils.Log(this, "Request content: ", content);
                Uri remoteVideo = new Uri(content);

                // Checks the uri integrity and validity
                if (remoteVideo.IsWellFormedOriginalString())
                {

                    // Checks if the video is a file on the local machine
                    if (remoteVideo.IsFile)
                    {                        
                        Utils.Log(this, "Request url is a local file: ", request.Url.AbsolutePath, "\nQuery: ", request.Url.Query);
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        response.StatusDescription = "Url is a local file.";
                    }
                    else
                    {
                        // Success
                        response.StatusCode = (int)HttpStatusCode.Accepted;

                        // Raises the event
                        if (OnHttpPost != null)
                            OnHttpPost(request, remoteVideo);
                    }
                }
                // Invalid url
                else
                {
                    Utils.Log(this, "Bad format.");
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    response.StatusDescription = "Invalid url..";
                }
            }
            // Invalid request
            else
            {
                Utils.Log(this, "Not a post request received.");
                response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                response.StatusDescription = "Processing only post, file request.";
            }

            // Closes the request, sending the response
            response.Close();
        }
    }
}
