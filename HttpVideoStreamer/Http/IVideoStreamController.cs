﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpVideoStreamer.Http
{
    public interface IVideoStreamController
    {
        TaskStatus Status { get; }

        int GetResult();
    }
}
